<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //Relación entre Post, User y country 
    public function posts(){
        return $this->hasManyThrough('App\Post', 'App\User');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPropertySoftdeleteToPost extends Migration
{ 
    //funcion para la creacion de la tabla post
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->softDeletes();  
        });
    } 
     //funcion para la eliminación de la tabla post
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}

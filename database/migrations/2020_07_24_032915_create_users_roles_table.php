<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersRolesTable extends Migration
{ 
    //funcion para la creacion de la tabla role_usuario donde se guardará la relación muchos a muchos de usuario a rol
    public function up()
    {
        Schema::create('role_user', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('role_id');
            $table->timestamps();
        });
    } 
    //funcion para la eliminación de la tabla role_user
    public function down()
    {
        Schema::dropIfExists('role_user');
    }
}

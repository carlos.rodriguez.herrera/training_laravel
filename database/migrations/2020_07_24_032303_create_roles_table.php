<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{ 
     //funcion para la creacion de la tabla role
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
    } 
    //funcion para la eliminación de la tabla role
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
